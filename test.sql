-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-12-2015 a las 07:25:24
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
`id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `answer`
--

INSERT INTO `answer` (`id`, `question_id`, `text`) VALUES
(1, 1, 'no'),
(2, 1, 'si'),
(3, 1, 'a veces'),
(4, 2, 'menos de 18'),
(5, 2, 'mas de 18'),
(6, 2, 'no responde'),
(7, 3, 'si'),
(8, 3, 'no'),
(9, 3, 'solo cuando juega la seleccion'),
(10, 4, 'solo'),
(11, 4, 'de 2 a 5'),
(12, 4, 'mas de 5'),
(13, 5, 'ninguna'),
(14, 5, 'una'),
(15, 5, 'mas de una');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question`
--

CREATE TABLE IF NOT EXISTS `question` (
`id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `question`
--

INSERT INTO `question` (`id`, `survey_id`, `text`) VALUES
(1, 1, 'es feliz'),
(2, 1, 'cuantos años tiene'),
(3, 1, 'le gusta el futbol'),
(4, 1, 'cuantas personas viven en su casa'),
(5, 1, 'cuantas mascotas tiene en su casa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `survey`
--

CREATE TABLE IF NOT EXISTS `survey` (
`id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `survey`
--

INSERT INTO `survey` (`id`, `date_created`) VALUES
(1, '2015-12-28 02:30:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'pedro', 'pedro'),
(2, 'diego', 'diego'),
(3, 'juan', 'juan');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_answer`
--

CREATE TABLE IF NOT EXISTS `user_answer` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_answer`
--

INSERT INTO `user_answer` (`id`, `user_id`, `answer_id`) VALUES
(1, 1, 2),
(4, 1, 5),
(5, 1, 8),
(6, 1, 11),
(7, 1, 15),
(8, 2, 2),
(9, 2, 5),
(10, 2, 9),
(11, 2, 11),
(12, 2, 14),
(13, 3, 1),
(14, 3, 4),
(15, 3, 7),
(16, 3, 10),
(17, 3, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_survey`
--

CREATE TABLE IF NOT EXISTS `user_survey` (
`id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_survey` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_survey`
--

INSERT INTO `user_survey` (`id`, `id_user`, `id_survey`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `answer`
--
ALTER TABLE `answer`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `question`
--
ALTER TABLE `question`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `survey`
--
ALTER TABLE `survey`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_answer`
--
ALTER TABLE `user_answer`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_survey`
--
ALTER TABLE `user_survey`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id_2` (`id`), ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `answer`
--
ALTER TABLE `answer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `question`
--
ALTER TABLE `question`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `survey`
--
ALTER TABLE `survey`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `user_answer`
--
ALTER TABLE `user_answer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `user_survey`
--
ALTER TABLE `user_survey`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
